var should = require('chai').should(),
    expect = require('chai').expect,
    supertest = require('supertest'),
    api = supertest('https://api.chn-or-cbe.nade.dev.clientdigital.io/');
var authToken = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVlNmIzZDM4MmZhYmMxNDg5NTYxNTA4ZiIsImlhdCI6MTU4NDYwMzQ5M30.qHJ7PvsiMqB-c-gDa9cNWjnXKyttsPHGsR8D5lf-v7s";

var errorHighlighter = {
    errorBg : "\x1b[41m",
    successBg : "\x1b[42m",
    errorText : "\x1b[31m",
    successText : "\x1b[32m",
}

describe('User', function() {

    var location1;
    var location2;
    var location3;
    var locations = [location1,
        location2, location3];

    var privilegeId = '';
       

before(function(done) {
        api.get('/privilege-master')
            .set('Accept', 'application/x-www-form-urlencoded')
            .set('Authorization', authToken)
            .expect('Content-Type', /json/)
            .expect(200, done);

    });

it('should add privilege-master', function (done) {   
        api.post('/privilege-master/')
            .set('Accept', 'application/x-www-form-urlencoded')
            .set('Authorization', authToken)
            .send({
                "privilegeName": "Procurer",
                "privilegeShortCode": "PCR",
                "modules": [],
                "pages": [],
                "isDeleted": false,
                "status": true,
                })
            .expect(200)
            .end(function (err, res) {
                console.log('new privilege is created', res.body);
                done();
            });
    });
});






var should = require('chai').should(),
    expect = require('chai').expect,
    supertest = require('supertest'),
    api = supertest('https://api.chn-or-cbe.nade.dev.clientdigital.io/');
var authToken = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVlNmIzZDM4MmZhYmMxNDg5NTYxNTA4ZiIsImlhdCI6MTU4NDYwMzQ5M30.qHJ7PvsiMqB-c-gDa9cNWjnXKyttsPHGsR8D5lf-v7s";

var errorHighlighter = {
    errorBg : "\x1b[41m",
    successBg : "\x1b[42m",
    errorText : "\x1b[31m",
    successText : "\x1b[32m",
}

describe('User', function() {

    var location1;
    var location2;
    var location3;
    var locations = [location1,
        location2, location3];

    var stateId = '';
       

before(function(done) {
        api.get('/state')
            .set('Accept', 'application/x-www-form-urlencoded')
            .set('Authorization', authToken)
            .expect('Content-Type', /json/)
            .expect(200, done);

    });

it('should add state', function (done) {   
        api.post('/state/')
            .set('Accept', 'application/x-www-form-urlencoded')
            .set('Authorization', authToken)
            .send({
                 "countryId": "5f79e2042b0ca88943363aef",
                 "stateName": "Tamilnadu",
                 "stateCode": "TN",
                 "stateDescription": "State name is Tamilnadu",
                 "isDeleted": false,
                 "status": true,
                })
            .expect(200)
            .end(function (err, res) {
                console.log('new state is created', res.body);
                done();
            });
    });
});





var should = require('chai').should(),
    expect = require('chai').expect,
    supertest = require('supertest'),
    api = supertest('https://api.chn-or-cbe.nade.dev.clientdigital.io/');
var authToken = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVlNmIzZDM4MmZhYmMxNDg5NTYxNTA4ZiIsImlhdCI6MTU4NDYwMzQ5M30.qHJ7PvsiMqB-c-gDa9cNWjnXKyttsPHGsR8D5lf-v7s";

var errorHighlighter = {
    errorBg : "\x1b[41m",
    successBg : "\x1b[42m",
    errorText : "\x1b[31m",
    successText : "\x1b[32m",
}

describe('User', function() {

    var location1;
    var location2;
    var location3;
    var locations = [location1,
        location2, location3];

    var countryId = '';
       

before(function(done) {
        api.get('/countries')
            .set('Accept', 'application/x-www-form-urlencoded')
            .set('Authorization', authToken)
            .expect('Content-Type', /json/)
            .expect(200, done);

    });

it('should add country', function (done) {   
        api.post('/countries/')
            .set('Accept', 'application/x-www-form-urlencoded')
            .set('Authorization', authToken)
            .send({
                 "countryName": "Srilanka",
                 "countryCode": "LANKA",
                 "countryDescription": "Country name is Srilanka",
                 "isDeleted": "false",
                 "status": "true"
                })
            .expect(200)
            .end(function (err, res) {
                console.log('new country is created', res.body);
                done();
            });
    });
});




var should = require('chai').should(),
    expect = require('chai').expect,
    supertest = require('supertest'),
    api = supertest('https://api.chn-or-cbe.nade.dev.clientdigital.io/');
var authToken = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVlNmIzZDM4MmZhYmMxNDg5NTYxNTA4ZiIsImlhdCI6MTU4NDYwMzQ5M30.qHJ7PvsiMqB-c-gDa9cNWjnXKyttsPHGsR8D5lf-v7s";

var errorHighlighter = {
    errorBg : "\x1b[41m",
    successBg : "\x1b[42m",
    errorText : "\x1b[31m",
    successText : "\x1b[32m",
}

describe('User', function() {

    var location1;
    var location2;
    var location3;
    var locations = [location1,
        location2, location3];

    var userId = '';
       

before(function(done) {
        api.get('/users')
            .set('Accept', 'application/x-www-form-urlencoded')
            .set('Authorization', authToken)
            .expect('Content-Type', /json/)
            .expect(200, done);

    });

it('should add user', function (done) {   
        api.post('/users/')
            .set('Accept', 'application/x-www-form-urlencoded')
            .set('Authorization', authToken)
            .send({
                    "firstName": "shailash",
                    "lastName": "j",
                    "email": "shailash@clientdigital.net",
                    "password": "shailash123",
                    "userRoleId": "5f79ec902b0ca88943363f86",
                    "userRole": "admin",
                    "nadeUserId": "NU1006",
                    "phoneNumber": "9094163767",
                    "alternatePhoneNumber": "",
                    "address": "",
                    "districtId": "5f79e3072b0ca88943363b63",
                    "stateId" : "5f79e2912b0ca88943363b27",
                    "countryId" : "5f79e2042b0ca88943363aef",
                    "userPhoto": {},
                    "picture": "",
                    "role": "admin",
                })
            .expect(200)
            .end(function (err, res) {
                console.log('new user is created', res.body);
                done();
            });
    });
});





var should = require('chai').should(),
    expect = require('chai').expect,
    supertest = require('supertest'),
    api = supertest('https://api.chn-or-cbe.nade.dev.clientdigital.io/');
var authToken = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVlNmIzZDM4MmZhYmMxNDg5NTYxNTA4ZiIsImlhdCI6MTU4NDYwMzQ5M30.qHJ7PvsiMqB-c-gDa9cNWjnXKyttsPHGsR8D5lf-v7s";

var errorHighlighter = {
    errorBg : "\x1b[41m",
    successBg : "\x1b[42m",
    errorText : "\x1b[31m",
    successText : "\x1b[32m",
}

describe('User', function() {

    var location1;
    var location2;
    var location3;
    var locations = [location1,
        location2, location3];

    var userId = '';
       

before(function(done) {
        api.get('/user-role-master')
            .set('Accept', 'application/x-www-form-urlencoded')
            .set('Authorization', authToken)
            .expect('Content-Type', /json/)
            .expect(200, done);

    });

it('should add user role master', function (done) {   
        api.post('/user-role-master/')
            .set('Accept', 'application/x-www-form-urlencoded')
            .set('Authorization', authToken)
            .send({
                   "privilegeId": "5f79de872b0ca889433639d0",
                    "roleName": "procurement team",
                    "roleShortName": "PTM",
                    "roleDescription": "This is Procurement team member",
                    "isDeleted": false,
                    "status": true,
                })
            .expect(200)
            .end(function (err, res) {
                console.log('new user role is created', res.body);
                done();
            });
    });
});





var should = require('chai').should(),
    expect = require('chai').expect,
    supertest = require('supertest'),
    api = supertest('https://api.chn-or-cbe.nade.dev.clientdigital.io/');
var authToken = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVlNmIzZDM4MmZhYmMxNDg5NTYxNTA4ZiIsImlhdCI6MTU4NDYwMzQ5M30.qHJ7PvsiMqB-c-gDa9cNWjnXKyttsPHGsR8D5lf-v7s";

var errorHighlighter = {
    errorBg : "\x1b[41m",
    successBg : "\x1b[42m",
    errorText : "\x1b[31m",
    successText : "\x1b[32m",
}

describe('User', function() {

    var location1;
    var location2;
    var location3;
    var locations = [location1,
        location2, location3];

    var vehicleId = '';
       

before(function(done) {
        api.get('/vehicles')
            .set('Accept', 'application/x-www-form-urlencoded')
            .set('Authorization', authToken)
            .expect('Content-Type', /json/)
            .expect(200, done);

    });

it('should add user products', function (done) {   
        api.post('/vehicles/')
            .set('Accept', 'application/x-www-form-urlencoded')
            .set('Authorization', authToken)
            .send({
                    "vehicleTypeId": "5f904b1d3a287221dae5763b",
                    "registrationNumber": "TN 52 A 5263",
                    "isInsuranceAvailable": true,
                    "insuranceValidUpto": "2023-10-16",
                    "insuranceCopy": "",
                    "isRCAvailable": true,
                    "rcCopy": "",
                    "isFCAvailable": true,
                    "fcValidUpto": "2023-05-23",
                    "fcCopy": "",
                    "isPermitAvailable": true,
                    "permitType": "National",
                    "ownerName": "Nade",
                    "registeredYear": "2009-01-04",
                    "isDeleted": false,
                    "status": true,
                })
            .expect(200)
            .end(function (err, res) {
                console.log('new product is vehicle', res.body);
                done();
            });
    });
});